import { useState } from "react";
import "./App.css";
let object1 = [
  { name: "Js", checked: false },
  { name: "Html", checked: false },
  { name: "Css", checked: false },
  { name: "Ts", checked: false },
];
let object2 = [
  { name: "React", checked: false },
  { name: "Angular", checked: false },
  { name: "Svelte", checked: false },
  { name: "Vue", checked: false },
];
function App() {
  let [items1, setItemList1] = useState(JSON.parse(JSON.stringify(object1)));
  let [items2, setItemList2] = useState(JSON.parse(JSON.stringify(object2)));

  function dblLeft() {
    let itemsAll = [...items1, ...items2];

    setItemList1(itemsAll);
    setItemList2([]);
  }
  function dblRight() {
    let itemsAll = [...items1, ...items2];
    setItemList2(itemsAll);
    setItemList1([]);
  }
  function moveLeftSide() {
    let item2 = [...items2];
    let checked = item2.reduce((acc, item) => {
      if (item.checked) {
        acc.push({ ...item, checked: false });
      }
      return acc;
    }, []);

    let unchecked = items2.filter((item) => {
      if (!item.checked) return item;
    });

    setItemList1([...items1, ...checked]);
    setItemList2(unchecked);
  }

  function moveRightSide() {
    let item1 = [...items1];
    let checked = item1.reduce((acc, item) => {
      if (item.checked) {
        acc.push({ ...item, checked: false });
      }
      return acc;
    }, []);
    let unchecked = item1.filter((item) => {
      if (!item.checked) {
        return item;
      }
    });
    setItemList2([...items2, ...checked]);
    setItemList1(unchecked);
  }

  function update(e, index, list, listNum) {
    // console.log(index);
    // console.log(list[index]);

    if (listNum == 2) {
      let temp = [...list];
      temp[index].checked = e.target.checked;
      setItemList2(temp);
    }
    if (listNum == 1) {
      let temp = [...list];
      temp[index].checked = e.target.checked;
      setItemList1(temp);
    }
  }

  function stateChange(list){
     let result = list.some((item) => item.checked === true)
     return result
  }

  return (
    <>
      <h1 className="text-3xl font-bold">Transfer List</h1>
      <div className="w-[80%] h-[50%] border-2 border-black  flex justify-between self-center ml-28">
        <ul className="w-[30%]">
          {items1.map((item, index) => (
            <li className=" gap-2 p-3">
              <label key={item} className="label">
                <input
                  type="checkbox"
                  checked={item.checked}
                  className="m-1"
                  onChange={(e) => update(e, index, items1, 1)}
                />
                {item.name}
              </label>
            </li>
          ))}
        </ul>
        <ul className="w-[20%] flex flex-col border-l-2 border-r-2 border-black">
          <button className="border-2 border-black mx-5 my-2 disabled:bg-gray-500 disabled:border-gray-500" onClick={dblLeft} disabled={Object.keys(items2).length===0}>
            {"<<"}
          </button>
          <button
            className="border-2 border-black mx-5 my-2 disabled:bg-gray-500 disabled:border-gray-500"
            onClick={moveLeftSide}
            disabled={!stateChange(items2)}
          >
            {"<"}
          </button>
          <button
            className="border-2 border-black mx-5 my-2 disabled:bg-gray-500 disabled:border-gray-500"
            onClick={moveRightSide}
            disabled={!stateChange(items1)}
          >
            {">"}
          </button>
          <button
            className="border-2 border-black mx-5 my-2 disabled:bg-gray-500 disabled:border-gray-500"
            onClick={dblRight}
            disabled={Object.keys(items1).length===0}
          >
            {">>"}
          </button>
        </ul>
        <ul className="w-[30%]">
          {items2.map((item, index) => (
            <li className="flex gap-2 p-3">
              <label key={item} className="label">
                <input
                  type="checkbox"
                  checked={item.checked}
                  className="m-1"
                  onChange={(e) => update(e, index, items2, 2)}
                />
                {item.name}
              </label>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default App;
